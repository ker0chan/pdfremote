var _port = 12345;
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static('static'));
app.use(express.static('png'));

app.get('/', function(req, res){
  res.sendFile(__dirname + '/index.html');
});
app.get('/display.html', function(req, res){
  res.sendFile(__dirname + '/display.html');
});
app.get('/comments.html', function(req, res){
  res.sendFile(__dirname + '/comments.html');
});

io.on('connection', function(socket){
  console.log('a user connected');

socket.on('prev', function(p){
    console.log("setting prev page");
    io.emit("prev");
  });
socket.on('next', function(p){
    console.log("setting next page");
    io.emit("next");
  });
  socket.on('page', function(p){
    console.log("setting page "+p);
    io.emit("page", p);
  });

});

http.listen(_port, function(){
  console.log('listening on *:'+_port);
});
